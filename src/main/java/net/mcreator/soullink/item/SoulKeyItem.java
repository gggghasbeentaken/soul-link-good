
package net.mcreator.soullink.item;

import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.CreativeModeTab;

public class SoulKeyItem extends Item {
	public SoulKeyItem() {
		super(new Item.Properties().tab(CreativeModeTab.TAB_MISC).durability(2).fireResistant().rarity(Rarity.EPIC));
	}
}
