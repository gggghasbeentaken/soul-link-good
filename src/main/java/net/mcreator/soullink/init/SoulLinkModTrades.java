
/*
*	MCreator note: This file will be REGENERATED on each build.
*/
package net.mcreator.soullink.init;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.village.VillagerTradesEvent;
import net.minecraftforge.common.BasicItemListing;

import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.entity.npc.VillagerProfession;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.FORGE)
public class SoulLinkModTrades {
	@SubscribeEvent
	public static void registerTrades(VillagerTradesEvent event) {
		if (event.getType() == VillagerProfession.CLERIC) {
			event.getTrades().get(4).add(new BasicItemListing(new ItemStack(Blocks.RESPAWN_ANCHOR), new ItemStack(Items.TOTEM_OF_UNDYING),
					new ItemStack(SoulLinkModItems.SOUL_PICKLE_SHARD.get(), 3), 3, 100, 0.05f));
		}
	}
}
