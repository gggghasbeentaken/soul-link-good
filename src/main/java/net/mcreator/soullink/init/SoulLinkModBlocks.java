
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.soullink.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.Block;

import net.mcreator.soullink.block.SoulTotemBlock;
import net.mcreator.soullink.SoulLinkMod;

public class SoulLinkModBlocks {
	public static final DeferredRegister<Block> REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCKS, SoulLinkMod.MODID);
	public static final RegistryObject<Block> SOUL_TOTEM = REGISTRY.register("soul_totem", () -> new SoulTotemBlock());
}
