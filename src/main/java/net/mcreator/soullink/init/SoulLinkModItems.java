
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.soullink.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.BlockItem;

import net.mcreator.soullink.item.SoullItem;
import net.mcreator.soullink.item.SoulPickleShardItem;
import net.mcreator.soullink.item.SoulKeyItem;
import net.mcreator.soullink.SoulLinkMod;

public class SoulLinkModItems {
	public static final DeferredRegister<Item> REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, SoulLinkMod.MODID);
	public static final RegistryObject<Item> SOUL_TOTEM = block(SoulLinkModBlocks.SOUL_TOTEM, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final RegistryObject<Item> SOUL_PICKLE_SHARD = REGISTRY.register("soul_pickle_shard", () -> new SoulPickleShardItem());
	public static final RegistryObject<Item> SOULL = REGISTRY.register("soull", () -> new SoullItem());
	public static final RegistryObject<Item> SOUL_KEY = REGISTRY.register("soul_key", () -> new SoulKeyItem());

	private static RegistryObject<Item> block(RegistryObject<Block> block, CreativeModeTab tab) {
		return REGISTRY.register(block.getId().getPath(), () -> new BlockItem(block.get(), new Item.Properties().tab(tab)));
	}
}
