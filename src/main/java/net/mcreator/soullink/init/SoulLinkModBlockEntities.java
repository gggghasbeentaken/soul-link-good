
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.soullink.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.Block;

import net.mcreator.soullink.block.entity.SoulTotemBlockEntity;
import net.mcreator.soullink.SoulLinkMod;

public class SoulLinkModBlockEntities {
	public static final DeferredRegister<BlockEntityType<?>> REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCK_ENTITY_TYPES,
			SoulLinkMod.MODID);
	public static final RegistryObject<BlockEntityType<?>> SOUL_TOTEM = register("soul_totem", SoulLinkModBlocks.SOUL_TOTEM,
			SoulTotemBlockEntity::new);

	private static RegistryObject<BlockEntityType<?>> register(String registryname, RegistryObject<Block> block,
			BlockEntityType.BlockEntitySupplier<?> supplier) {
		return REGISTRY.register(registryname, () -> BlockEntityType.Builder.of(supplier, block.get()).build(null));
	}
}
